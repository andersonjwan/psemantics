import random

from .options import Options, StatementType
from .program import Program
from .result import Result
from .statements import Allocation, Assignment, Deallocation, Declaration

from typing import List

def psemantics(
    options: Options
) -> Result:
    """Generate a C program with an emphasis on pointer semantics.
    """

    random.seed(options.seed)

    # validate statement probabilities
    if sum([x.prob for x in options.stmts]) != 1.0:
        raise Exception("statement probability must sum to 1.0")

    # validate unique statements
    if len([x.stype for x in options.stmts]) != len(set([x.stype for x in options.stmts])):
        raise Exception("duplicate statement types not permitted")

    selections = random.choices(
        population=[x.stype for x in options.stmts],
        weights=[x.prob for x in options.stmts],
        k=options.stmt_count
    )

    return Result(
        seed=options.seed,
        program=_generate_program(selections)
    )

def _generate_program(selection: List[StatementType]) -> Program:
    """Generate a valid C program
    """

    stmts = []
    for stmt in selection:
        stmts.append(_generate_statement(stmt))

    return Program(statements=stmts)


def _generate_statement(stype: StatementType):
    """Generate the appropriate statement.
    """

    match stype:
        case StatementType.ALLOCATION:
            return Allocation()
        case StatementType.ASSIGNMENT:
            return Assignment()
        case StatementType.DEALLOCATION:
            return Deallocation()
        case StatementType.DECLARATION:
            return Declaration()
