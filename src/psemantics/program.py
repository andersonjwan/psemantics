from attr import frozen
from .statements import Allocation, Assignment, Deallocation, Declaration
from typing import List, Optional, Union

Stmt = Union[
    Allocation,
    Assignment,
    Deallocation,
    Declaration
]

@frozen()
class Program():
    """Represent a C program.
    """

    statements: Optional[List[Stmt]]

    def __str__(self):
        """Return a program as a string.
        """
        s = ""

        for stmt in self.statements:
            s += str(stmt) + "\n"

        return s
