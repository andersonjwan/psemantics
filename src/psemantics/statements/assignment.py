class Assignment():
    """Represent an assignment statement.

    Attributes:
    """

    scope: int = 0

    def __str__(self) -> str:
        """Return the assignment as a string.
        """

        return "ASSIGNMENT STATEMENT;"
