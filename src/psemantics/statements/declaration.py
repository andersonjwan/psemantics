class Declaration():
    """Represent a declaration statement.

    Attributes:
    """

    scope: int = 0

    def __str__(self) -> str:
        """Return the declaration statement as a string.
        """

        return "DECLARATION STATEMENT;"
