class Allocation():
    """Represent an allocation statement.

    Attributes:
    """

    scope: int = 0

    def __str__(self) -> str:
        """Return the allocation statement as a string.
        """

        return "ALLOCATION STATEMENT;"
