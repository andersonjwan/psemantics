class Deallocation():
    """Represent a deallocation statement.

    Attributes:
    """

    scope: int = 0

    def __str__(self) -> str:
        """Return the deallocation statement as a string.
        """

        return "DEALLOCATION STATEMENT;"
