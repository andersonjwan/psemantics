from attr import field, frozen
from typing import List

from .statements import Assignment
from .program import Program

@frozen
class Result():
    """A resulting program (i.e., a series of statements).

    Attributes:
        seed: The used seed of to generate the result
        stmts: The list of statements
    """

    seed: int
    program: Program
