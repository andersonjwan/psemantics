import random

from attr import field, frozen
from dataclasses import dataclass
from enum import Enum
from typing import List

class StatementType(Enum):
    """Types of valid statements.
    """

    ASSIGNMENT = 1
    DECLARATION = 2
    ALLOCATION = 3
    DEALLOCATION = 4


@dataclass
class Statement():
    """A statement and associated probability.
    """

    stype: StatementType
    prob: float


def _seed_factory() -> int:
    return random.randint(0, 2 ** 32 - 1)


@frozen()
class Options():
    """General set of options for generating pointer semantic examples.

    Attributes:
        seed: Initial seed of Random Number Generator (RNG)
        stmts: Map of statement names (str) and associated probability of
            generating the statement (float) to be included
        stmt_count: The number of statements to generate
    """

    seed: int = field(factory=_seed_factory)
    stmts: List[Statement] = field(factory=list)
    stmt_count: int = field(default=0)
