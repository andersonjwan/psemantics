# Pointer Semantics Generator

A tool to generate simple C code with an emphasis on pointer semantics.

## Getting Started

To get started, this tool uses [Poetry](https://python-poetry.org/) to manage package dependencies and build. Therefore, please ensure the tool is installed with the command `poetry --version`.

With Poetry installed, the package and necessary dependencies can be installed with the following command:

```bash
poetry install
```

An example can be ran with the following command:

```bash
poetry run python examples/...
```

## Author's Notes

If you have any questions, please feel free to contact me via email at jwande18@asu.edu