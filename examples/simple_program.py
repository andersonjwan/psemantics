from psemantics import Options, Statement, StatementType as st
from psemantics.statements import Declaration
from psemantics.psemantics import psemantics

options = Options(
    stmts=[
        Statement(stype=st.ALLOCATION, prob=0.2),
        Statement(stype=st.ASSIGNMENT, prob=0.5),
        Statement(stype=st.DEALLOCATION, prob=0.1),
        Statement(stype=st.DECLARATION, prob=0.2),
    ],
    stmt_count=10
)

if __name__ == "__main__":
    """Simple ten statement program of all statement types supported.
    """

    res = psemantics(options)
    print(str(res) + "\n")
    print(res.program)
